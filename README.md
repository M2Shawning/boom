**Boom**

The #1 Minecraft Plugin for Making Your Friends Go Boom



**What Is This?**

Some stupid plugin I wrote that will cause an explosion at a random player's crosshair every two minutes. Here is an example [video](https://youtu.be/o38gTywEFnA).

**Yes, But Why?**

Achievement Hunter recently released a video where they randomly exploded every two minutes. [See video](https://roosterteeth.com/watch/let-s-play-minecraft-2020-blow-up-6-5). Matt commented about possibly triggering explosions on crosshairs in a future video. I said fuck, I'll do that.

**How Do I Use Boom?**
- Place `boom-1.0-SNAPSHOT.jar` in the `plugins` folder of your Bukkit/Spigot/PaperMC server.
- In game, type `/boom start` to start the timer
- Wait two minutes and observe

**Quick Link To Download**

[boom-1.0-SNAPSHOT.jar](/uploads/5348c1e472f1d8193caaa3002106b49f/boom-1.0-SNAPSHOT.jar)