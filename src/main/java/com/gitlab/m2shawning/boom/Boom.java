package com.gitlab.m2shawning.boom;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Boom extends JavaPlugin {
    public boolean boomEnabled = false;

    @Override
    public void onEnable(){
        if (!(Bukkit.getVersion().contains("MC: 1.15"))) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Boom]Error: Unsupported Version");
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Boom]Please Use Version 1.15");
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Boom]Disabling Boom...");
            Bukkit.getPluginManager().disablePlugin(this);
        }

        this.getCommand("Boom").setExecutor(new CommandBoom(this));
    }

    @Override
    public void onDisable(){

    }

    public void boomScheduler() {
        new BukkitRunnable() {
            int counter = 120;

            public void run() {
                if (!boomEnabled) {
                    this.cancel();
                    return;
                }

                if (Bukkit.getServer().getOnlinePlayers().isEmpty()) {
                    boomEnabled = false;
                    getLogger().info(ChatColor.GREEN + "Server Empty, Stopping Boom Session");
                    this.cancel();
                    return;
                }

                if (this.counter == 0) {
                    List<Player> playerList = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());

                    int listSize;
                    int randomInt;
                    Player player;
                    Block targetBlock;

                    do {
                        listSize = playerList.size();
                        randomInt = new Random().nextInt(listSize);
                        player = playerList.get(randomInt);

                        boolean alivePlayers = false;
                        for (int i = 0; i < listSize; i++) {
                            if (!playerList.get(i).isDead()) {
                                alivePlayers = true;
                                break;
                            }
                        }
                        if (!alivePlayers) {
                            boomScheduler();
                            this.cancel();
                            getServer().broadcastMessage(ChatColor.GREEN + "How the actual f*ck is everyone dead?");
                            return;
                        }

                        targetBlock = player.getTargetBlockExact(255);
                        if (targetBlock == null) {
                            targetBlock = player.getLocation().getBlock();
                        }

                    } while(Bukkit.getWorld(player.getWorld().getUID()) == null || player.isDead());

                    try {
                        Bukkit.getWorld(player.getWorld().getUID()).createExplosion(targetBlock.getX(), targetBlock.getY(), targetBlock.getZ(), 6F, false, true);
                    } catch (NullPointerException e) {
                        getLogger().severe("An error occurred while creating an explosion");
                        boomScheduler();
                        this.cancel();
                        return;
                    }

                    getServer().broadcastMessage(ChatColor.GREEN + "BOOM!");
                    boomScheduler();
                    this.cancel();
                } else if (this.counter <= 3) {
                    getServer().broadcastMessage(ChatColor.GREEN + Integer.toString(this.counter) + "...");
                }

                this.counter--;
            }
        }.runTaskTimer(this, 0L, 20L);
    }
}

