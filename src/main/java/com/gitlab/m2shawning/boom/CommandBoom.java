package com.gitlab.m2shawning.boom;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandBoom implements CommandExecutor {
    private Boom plugin;

    public CommandBoom(Boom plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String l, String[] args) {
        if (!(command.getName().equalsIgnoreCase("boom"))) {
            return true;
        }

        // If no arguments or 1 argument which is help
        if ((args.length == 1 && args[0].equalsIgnoreCase("help")) || (args.length == 0)) {
            sender.sendMessage(ChatColor.GREEN + "[Boom Help]");
            sender.sendMessage(ChatColor.GREEN + "/boom start");
            sender.sendMessage(ChatColor.GREEN + "/boom stop");
            return true;
        } else if (args.length == 1 && args[0].equalsIgnoreCase("start")) {
            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Did someone call Team Nice Dynamite?");
            plugin.boomEnabled = true;
            plugin.boomScheduler();
            return true;

        } else if (args.length == 1 && args[0].equalsIgnoreCase("stop")) {
            plugin.getServer().broadcastMessage(ChatColor.GREEN + "Whut is this?!?!");
            plugin.boomEnabled = false;
            return true;
        }

        return false;
    }
}